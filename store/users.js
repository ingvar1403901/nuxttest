export const state = () => ({
  users: [],
  usersFavoriteListId: [],
});
export const actions = {
  // Получение списка всех персонажей
  async actionsUsers(ctx) {
    let res = await fetch("https://rickandmortyapi.com/api/character", {});
    const productsArray = await res.json();
    ctx.commit('setUsers', productsArray.results);
  },
  // Запись массива добавленных в избранное
  async actionsUsersFavorites(ctx, userId) {
    if (userId === undefined) userId = [];
    ctx.commit('setUsersFavoritesList', userId);
  },
  // Запись нового аватара
  async actionsChangeAvatar(ctx, value) {
    ctx.commit('setChangeAvatar', value);
  },
};
export const mutations = {
  setUsers(state, users) {
    state.users = users;
  },
  setUsersFavoritesList(state, userId) {
    if(Array.isArray(userId)){
      state.usersFavoriteListId = state.usersFavoriteListId.concat(userId);
    }
    else{
      let index = state.usersFavoriteListId.indexOf(userId);
      if (index !== -1) {
        state.usersFavoriteListId.splice(index, 1);
      }else{
        state.usersFavoriteListId.push(userId);
      }
    }
  },
  setChangeAvatar(state, value) {
    state.users.find(t => t.id === Number(value.id)).image = value.url;
  },
};
export const getters = {
  // получение данных по товарам
  getUsers: s => s.users,
  // получение данных по конкретному персонажу
  getUsersId: (state) => (myArgument) => {
    return state.users.find(t => t.id === myArgument);
  },
  // получение списка ID избранных персонажей
  getUsersFavoriteListId(state) {
    return state.usersFavoriteListId;
  },
};
